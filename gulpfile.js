'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var open = require('gulp-open');

var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gulpConcat = require('gulp-concat');
var eslint = require('gulp-eslint');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var cssmin = require('gulp-cssmin');
var uncss = require('gulp-uncss');
var htmlmin = require('gulp-htmlmin');
var zopfli = require('gulp-zopfli');
var runSeq = require('run-sequence')

var config = {
  port: 3333,
  devBaseUrl: 'http://localhost',
  paths: {
    html: './src/*.html',
    css: './src/css/**/*.css',
    vendorCSS: [
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
      'node_modules/toastr/toastr.css',
    ],
    js: './src/js/**/*.js',
    vendorJS: [
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
      'node_modules/toastr/toastr.js',
    ],
    images: './src/**/*',
    mainJS: './src/app.js',
    dist: './dist'
  }
};

gulp.task('connect', function() {
  connect.server({
    root: ['dist'],
    port: config.port,
    base: config.devBaseUrl,
    livereload: true
  });
});

gulp.task('open', ['connect'], function() {
  gulp.src('dist/index.html')
    .pipe(open({uri: config.devBaseUrl + ':' + config.port + '/'}));
});

gulp.task('html', function() {
  gulp.src(config.paths.html)
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(config.paths.dist))
    .pipe(connect.reload());
});

gulp.task('css', function() {
  gulp.src(config.paths.css)
    .pipe(gulpConcat('bundle.css'))
    .pipe(cssmin())
    .pipe(gulp.dest(config.paths.dist + '/css'))
    .pipe(connect.reload());
});

gulp.task('vendorCSS', function() {
  gulp.src(config.paths.vendorCSS)
    .pipe(gulpConcat('vendor-bundle.css'))
    .pipe(cssmin())
    .pipe(gulp.dest(config.paths.dist + '/css'))
    .pipe(connect.reload());
});

gulp.task('mainJS', function() {
  browserify(config.paths.mainJS)
    .bundle()
    .on('error', console.error.bind(console))
    .pipe(source('main.js'))
    .pipe(gulp.dest(config.paths.dist + '/js'))
    .pipe(connect.reload());
});

gulp.task('js', function() {
  gulp.src(config.paths.js)
    .pipe(gulpConcat('bundle.js'))
    .pipe(gulp.dest(config.paths.dist + '/js'))
    .pipe(connect.reload());
});

gulp.task('vendorJS', function() {
  gulp.src(config.paths.vendorJS)
    .pipe(gulpConcat('vendor-bundle.js'))
    .pipe(gulp.dest(config.paths.dist + '/js'));
});

gulp.task('images', function() {
  gulp.src(config.paths.images)
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(config.paths.dist + '/images'));
});

gulp.task('lint', function() {
  return gulp.src(config.paths.js)
    .pipe(eslint({config: 'eslint.config.json'}))
    .pipe(eslint.format());
});

gulp.task('watch', function() {
  gulp.watch(config.paths.html, ['html']);
  gulp.watch(config.paths.css, ['css']);
  gulp.watch(config.paths.vendorCSS, ['vendorCSS']);
  gulp.watch(config.paths.mainJS, ['mainJS', 'lint']);
  gulp.watch(config.paths.js, ['js', 'lint']);
});

gulp.task('default', ['html', 'css', 'vendorCSS', 'mainJS', 'js', 'vendorJS', 'images', 'lint', 'open', 'watch']);

gulp.task('heroku:production', function() {
  return runSeq('html', 'css', 'vendorCSS', 'mainJS', 'js', 'vendorJS', 'images');
});
