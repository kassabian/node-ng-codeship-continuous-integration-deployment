(function() {
  'use strict';

  angular.module('cidApp')
    .controller('HomeController', ['$scope', 'HomeFactory', function($scope, HomeFactory) {
      

      HomeFactory.getJobs().then(function(results) {
        console.log(results, 'jobs - hc');
        $scope.jobs = results;
      });
    }]);
})();
