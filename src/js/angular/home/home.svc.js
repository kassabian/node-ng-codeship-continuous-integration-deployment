(function() {
  'use strict';
  angular.module('cidApp')
    .factory('HomeFactory', ['$http', '$q', 'API_URL', function($http, $q, API_URL) {
      return {
        getJobs: function() {
          var deferred = $q.defer();
          $http.get(API_URL + 'jobs').then(function(results) {
            deferred.resolve(results.data.jobs);
          }, function(err) {
            deferred.reject(err);
          });
          return deferred.promise;
        }
      };
    }]);

})();
